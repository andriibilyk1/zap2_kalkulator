#include <include/zapis_do_pliku.h>
#include <iostream>
using namespace std;
void zapis(string nazwa_pliku, int w, int k, int **T2){
    ofstream plik;
    string base =  string(SOURCES_PATH) + "/config/";
     plik.open(string(base + nazwa_pliku).c_str());
     for(int i=0; i<w; i++){
         for(int j=0; j<k; j++){
             plik << T2[i][j] << " ";
         }
         plik << endl;
     }
}
void zapis_w(string nazwa_pliku, double x){
    ofstream plik;
    string base =  string(SOURCES_PATH) + "/config/";
     plik.open(string(base + nazwa_pliku).c_str());
     plik << x;
}
