#-------------------------------------------------
#
# Project created by QtCreator 2016-04-23T11:28:05
#
#-------------------------------------------------

QT       += core gui
CONFIG += console
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Kalkulator
TEMPLATE = app
DEFINES += SOURCES_PATH=\\\"$$PWD\\\"
INCLUDEPATH += include

SOURCES += src/main.cpp\
        src/mainwindow.cpp \
    src/dodawanie.cpp \
    src/mnozenie.cpp \
    src/odejmowanie.cpp \
    src/wczyt.cpp \
    src/wyznacznik.cpp \
    src/zapis_do_pliku.cpp \
    src/wypisz.cpp

HEADERS  += include/mainwindow.h \
    include/dodawanie.h \
    include/mnozenie.h \
    include/odejmowanie.h \
    include/wczyt.h \
    include/wyznacznik.h \
    include/zapis_do_pliku.h \
    include/wypisz.h

FORMS    += mainwindow.ui
