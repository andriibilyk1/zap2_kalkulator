#ifndef WCZYT_H
#define WCZYT_H
#include <iostream>
#include <fstream>
using namespace std;
void wczyt( int n, int m, int **T1);
void wczyt_1(string nazwa_pliku, int n, int m, int **T1);
void wczyt_2(string nazwa_pliku, int n, int m, int **T1, int **T2, int  **T3);
void wczyt_2mn(string nazwa_pliku,int n, int m, int w, int k, int **T1, int **T2, int **T3);
void wczyt_2od(string nazwa_pliku, int n, int m, int **T1, int **T2, int  **T3);
void wczyt_3( int n, int m, double **T1);
void wczyt_w(string nazwa_pliku, int n, int m, double **T1);
#endif // WCZYT_H
